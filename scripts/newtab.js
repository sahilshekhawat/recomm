// $("body").ready(function(){
//     $("body").load("http://searchprivacy.co", function(){
//         $(".bottom-section").innerHTML = "<h1>These are recommended urls</h1>" + $(".bottom-section").innerHtml;
//     }




// var getItem = function(itemId, index){
//     $.get(
//         APP_SERVER_URL + "/item/" + itemId.toString() + ".json",
//         function (itemData){
//             console.log(itemData);
//             var nested_title_class = '.card.' + index.toString() + '.trending div.content h4.ui.header';
//             console.log(nested_title_class);
//             $(nested_title_class).text(itemData.title);
//             console.log(itemData.title);
//             var nested_domain_class = '.card.' + index.toString() + '.trending div.content div.meta a.link';
//             $(nested_domain_class).text(itemData.url.split("/")[2]);
//
//             IMAGE_RESOLVER.resolve(itemData.url, function(result) {
//                 if(result) {
//                     console.log(result.image);
//                 } else {
//                     console.log("No Image found");
//                 }
//             })
//
//         }
//     );
// };
//
//
// var getTrending = function(){
//     $.get(
//         APP_SERVER_URL + TRENDING_SUB_URL,
//         function(data){
//             console.log(data);
//             for(var i=1; i<5; i++){
//                 console.log(data[i].toString());
//                 console.log(APP_SERVER_URL + "/item/" + data[i].toString() + ".json");
//                 getItem(data[i], i);
//             }
//         });
// };
//
// getTrending();

// var finalHtml = "";

// $.ajax("http://localhost:8080/getTrending", {success: function(result){
//     console.log(result);
//     var urls = JSON.parse(JSON.stringify(result));
//     $.each(urls, function (i, val) {

//         val = JSON.parse(JSON.stringify(val));
//         var cardHtml = "<div class=\"card\" id=\""+ i +"\">\n" +
//             "                    <div class=\"image\" style=\"background:url(\'" +
//             val["imageUrl"] +
//             "\') no-repeat center center\">\n" +
//             // "                        <img src=\"" +
//             // val["imageUrl"] +
//             // "\">\n" +
//             "                    </div>\n" +
//             "                    <div class=\"content\">\n" +
//             "                        <div class=\"header\">"+
//             val["title"] +
//             "</div>\n" +
//             "                        <div class=\"meta\">\n" +
//             "                            <a>Tech</a>\n" +
//             "                        </div>\n" +
//             "                        <div class=\"description\">\n" +
//             val["desc"] +
//             "                        </div>\n" +
//             "                    </div>\n" +
//             "                    <div class=\"extra content\">\n" +
//             // "                      <span class=\"right floated\">\n" +
//             // "                        Joined in 2013\n" +
//             // "                      </span>\n" +
//             "                                    <span>\n" +
//             "                        <i class=\"user icon\"></i>\n" +
//             "                        Trending\n" +
//             "                      </span>\n" +
//             "                    </div>\n" +
//             "                </div>";

//         console.log(val.imageUrl);

//         //document.getElementById(i).addEventListener("click", location.href=val.url);
//         // var a = val;


//         finalHtml += cardHtml;
//     });

//     $(".recomm").html(finalHtml);
//     console.log("inserted");
//     console.log(finalHtml);

//     $.each(urls, function(i, val){
//         $("#" + i).click(function () {
//             window.location.href = val.url;
//         })
//     });

//     $(".image").css("height", "200px");
//      // $("body").css("min-width", "100%");

// }});

//
// chrome.history.onVisited.addListener(function (HistoryItem result) {
//    console.log(result);
// });

// console.log("hi there");

// $('.startTime').text(1502531821);
// $('.endTime').text(1502964259);

var VIEW_COUNT = 0;
var TRENDING_VIEW_COUNT = 0;
var RECOMM_VIEW_COUNT = 0;
var TRENDING_DATA = [];
var RECOMM_DATA = [];
var YAHOO_API = "https://search.yahoo.com/search?p=";
var autocompleteAPI  = 'http://ao-auto-complete-628753188.us-west-1.elb.amazonaws.com/autocomplete/autocomplete'

showTrending();
showRecomm();

$("#searchinput").val("");
$("#searchinput").focus();

$(".searchbutton").click(function () {
    window.location = YAHOO_API + $("#searchinput").val().replace(" ", "+");
    $("#searchinput").val("");

});

$("#searchinput").keypress(function (e) {
    console.log(e);
    var key = e.which;
    if (key === 13){
        $(".autocompleterow").hide();
        $(".searchbutton").click();
    }
});

var trendingViewMoreDisable = false;

$(".firstviewmore").click(function () {
    console.log("view more clicked");
    if (trendingViewMoreDisable)
        return;

    TRENDING_VIEW_COUNT++;
    showTrending();

    trendingViewMoreDisable = true;
    setTimeout(function () {
        trendingViewMoreDisable = false;
    }, 2000);
});

var recommViewMoreDisabled = false;

$(".secondviewmore").click(function () {
    console.log("view more clicked");
    if (recommViewMoreDisabled)
        return;

    RECOMM_VIEW_COUNT++;
    showRecomm();

    recommViewMoreDisabled = true;
    setTimeout(function () {
        recommViewMoreDisabled = false;
    }, 2000);
});


function showTrending(){
    console.log("Showing trending");
    chrome.storage.local.get("trending", function (data) {
        var data = data["trending"];
        TRENDING_DATA = data;
        setCards(data, false);
    });
}

function showRecomm() {
    console.log("showing recomms");
    chrome.storage.local.get("recommurls", function (data) {
        var data = data["recommurls"];
        RECOMM_DATA = data;
        setCards(data, true);
    });
}

function handleHideEvents(divClass, row){
    if(divClass === ".recomm"){
        $(row).hide();
        $(row + "heading").hide();
    } else {
        setTimeout(function () {
            showTrending();
        }, 1000);
    }
}

function setCards(data, isRecomm){
    var row;
    var divClass;

    if (isRecomm){
        row = ".secondrow";
        divClass = ".recomm";
    } else {
        row = ".firstrow";
        divClass = ".trending";
    }

    console.log(divClass);
    console.log(data);


    if (!data || !data.length){
        handleHideEvents(divClass, row);
        return;
    } else {
        if (data.length < 4) {
            handleHideEvents(divClass, row);
            return;
        } else {
            $(row).show();
            $(row + "heading").show();
        }

        // Setting value for 4 cards
        if (divClass === ".recomm")
            VIEW_COUNT = RECOMM_VIEW_COUNT;
        else
            VIEW_COUNT = TRENDING_VIEW_COUNT;

        var startIndex = 0;
        if (data.length >= VIEW_COUNT * 4 + 4){
            startIndex = VIEW_COUNT * 4;
        } else {
            if (VIEW_COUNT > 0)
                startIndex = data.length - 4;
        }

        // if (data.length === startIndex + 4){
        //
        // }

        var count = 0;
        for (var i = startIndex; i < startIndex + 4; i++){
            count++;
            var div = ".card." + count.toString() + divClass;
            console.log(div);
            console.log(data[i]);

            if (data.length <= i){
                quitLoop = true;
                if(divClass === ".recomm"){
                    $(row).hide();
                    $(row + "heading").hide();
                }
                else {
                    setTimeout(function () {
                        showTrending();
                    }, 1000);
                }
            }

            console.log(divClass);
            console.log(count);
            console.log(i);

            // Hide loader
            $(div + " .ui.segment").hide();
            $(div + " .image").show();


            $(div + " .content .ui.header").text(data[i].title);
            $(div + " .content .ui.header").attr("href", data[i].url);

            $(div).unbind("click");
            $(div).click(function () {
                 window.location.href  = $(this)[0].childNodes[5].childNodes[1].attributes[2].nodeValue;
            });

            var domain = "";
            if (data[i].url.substr(0, 4) === "http"){
                domain = data[i].url.split("/")[2];
            } else {
                domain = data[i].url.split("/")[0];
            }

            if (domain.substr(0, 3) === "www"){
                domain = domain.substr(4, domain.length);
            }

            $(div + " .content .meta .link").text(domain);
            $(div + " .content .meta .link").attr("href", "http://" + domain);

            if (data[i].image_url){
                $(div + " .image").show();
                $(div + " .image").css("background-image","url('" + data[i].image_url + "')");
                $(div + " .content .description").hide();
                $(div + " .image").attr("href", data[i].url);
            } else {
                $(div + " .image").hide();
                if (data[i].description){
                    $(div + " .content .description").show();
                    $(div + " .content .description").text(data[i].description);
                    $(div + " .content .description").attr("href", data[i].url);
                } else {
                    $(div + " .content .description").hide();
                    $(div + " .ui.segment").show();
                    $(div + " .image").hide();
                }
            }
        }
    }
}


$(".history-button").click(function () {
    var startTime = parseFloat($('.startTime').val());
    var endTime = parseInt($('.endTime').val());
    console.log(startTime);
    // console.log(startTime.length);
    // console.log(endTime);
    // console.log(endTime.length);

    // startTime = parseInt(startTime);
    // endTime = parseInt(endTime);
    chrome.history.search({text: '' , maxResults: 1000, startTime: startTime, endTime: endTime}, function(data) {
        data.forEach(function(page) {
            console.log(page.url);
            $(".history-content").html($(".history-content").html() + page.url  + "," + page.lastVisitTime + "</br>");
        });
    });
});


function loadAC(data, obj1) {
    try {
        document.getElementsByClassName('autolist')[0].innerHTML = "";
        var autoulobj = document.getElementsByClassName('autolist')[0];
        if (typeof(data[obj1]) !== "undefined" && data[obj1] != "") {
            document.getElementsByClassName('autolist')[0].style.display = "block";
            for (var i = 0; i < data[obj1].length; i++) {
                var autoliobj = document.createElement('li');
                autoliobj.classList.add('autolistli');
                autoliobj.classList.add('tt-suggestion');
                autoliobj.innerHTML = "<div class=\"item\">\n" +
                    "    <div class=\"content\">\n" +
                    "         <a class=\"header autolistvalues\" id=\"autoobj\">"+  data[obj1][i]  +"</a>\n" +
                    "     </div>\n";
                autoulobj.appendChild(autoliobj);

                document.getElementsByClassName('autolistvalues')[0].classList.add('autoactive');

                autoliobj.getElementsByClassName('autolistvalues')[0].onclick = function (e) {
                    document.getElementById('searchinput').value = this.innerHTML;
                    var urlx =  this.innerHTML;
                    document.getElementsByClassName('autolist')[0].style.display = "none";
                    yahoored(urlx);
                };

                autoliobj.getElementsByClassName('autolistvalues')[0].onmouseover = function (e) {
                    var listobj = document.getElementsByClassName('autolistvalues');
                    for (i = 0; i < listobj.length; i++) {
                        listobj[i].classList.remove('autoactive');
                    }
                    this.classList.add('autoactive');
                    document.getElementById('searchinput').value = this.innerHTML;
                }
            }
            //document.querySelectorAll(".searchtext")[0].appendChild(autoulobj);
        }
        else {
            document.getElementsByClassName('autolist')[0].style.display = "none";
        }
    }
    catch (e){
        console.log('error: ', e);
    }
}

function callXHR(url, cb, p){
    try{
        var xmlhttp = new XMLHttpRequest();
        var url = url;

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var myArr = JSON.parse(xmlhttp.responseText);
                if(typeof cb == "function"){
                    cb(myArr);
                }else{
                    if(p){
                        window[cb](myArr, p);
                    }
                    else window[cb](myArr);
                }
            }
        };

        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
    catch (e){
        console.log('error: ', e);
    }
}


function yahoored(inputsearchfunc) {
    var searchresult = inputsearchfunc.replace(" ", "+");
    window.location = YAHOO_API + searchresult;
}


initAC();


function initAC(){
    var tempc;
    // initialize autocomplete
    try{
        var t1obj = document.getElementById('searchinput');
        t1obj.onkeyup = function (e) {
            e.stopPropagation();
            // document.getElementsByClassName('autolist')[0].style.display = 'none';
            if (e.keyCode != 40 && e.keyCode != 38 && t1obj.value != "") {
                tempc = e.target.value;
                var urlx = autocompleteAPI + '?kwd=' + encodeURIComponent(t1obj.value) + '&count=5';
                callXHR(urlx, 'loadAC', tempc);

                document.getElementsByClassName('autolist')[0].style.display = 'block';
            }
            else if (t1obj.value == "") {
                document.getElementsByClassName('autolist')[0].style.display = 'none';
            }
            else if((e.keyCode == 40 || e.keyCode == 38) && t1obj.value != ""){
                var autoul = document.getElementsByClassName('autolistli');
                var autoulobj = document.getElementsByClassName('autolist')[0];
                var y = document.body.scrollTop;
                if (e.keyCode == '38') {
                    e.preventDefault();
                    if (autoul[0].getElementsByClassName('autolistvalues')[0].classList.contains('autoactive')) {
                        var lastobj = autoul.length - 1;
                        document.querySelector('.autoactive').classList.remove('autoactive');
                        autoul[lastobj].getElementsByClassName('autolistvalues')[0].classList.add('autoactive');
                        document.getElementById('searchinput').value = document.querySelector('.autoactive').innerHTML;
                    }
                    else {
                        var activeobj = document.querySelector('.autoactive');
                        document.querySelector('.autoactive').classList.remove('autoactive');
                        activeobj.parentNode.previousSibling.getElementsByClassName('autolistvalues')[0].classList.add('autoactive');
                        document.getElementById('searchinput').value = document.querySelector('.autoactive').innerHTML;
                    }
                }
                else if (e.keyCode == '40') {
                    var lastobj = autoul.length - 1;
                    if (autoul[lastobj].getElementsByClassName('autolistvalues')[0].classList.contains('autoactive')) {
                        autoul[lastobj].getElementsByClassName('autolistvalues')[0].classList.remove('autoactive');
                        autoul[0].getElementsByClassName('autolistvalues')[0].classList.add('autoactive');
                        document.getElementById('searchinput').value = document.querySelector('.autoactive').innerHTML;
                    }
                    else {
                        var activeobj = document.querySelector('.autoactive');
                        document.querySelector('.autoactive').classList.remove('autoactive');
                        activeobj.parentNode.nextSibling.getElementsByClassName('autolistvalues')[0].classList.add('autoactive');
                        document.getElementById('searchinput').value = document.querySelector('.autoactive').innerHTML;
                    }
                }
            }
        };
        t1obj.onkeypress = function(evt){
            var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
            var autoobj = document.getElementsByClassName('autolist')[0];
            if (keyCode == 13) {
                var autoactiveobj = document.querySelectorAll('.autoactive')[0];
                // if (autoobj.getElementsByClassName('autolistvalues').length > 0) {
                //     var txt = autoactiveobj.innerHTML;
                //     document.getElementsByClassName('autolist')[0].style.display = "none !important";
                //     document.getElementById('s1').value = txt;
                //     var urlx = replaceUrlParam(window.location.href, 'q', txt);
                //     window.location.href = urlx;

                // }
                var txt = document.getElementById('searchinput').value;
                document.getElementsByClassName('autolist')[0].style.display = "none !important";
                var urlx = replaceUrlParam(window.location.href, 'q', txt);
                window.location.href = urlx;
                // }
            }
        }
    }
    catch(e){
        console.log('error:', e);
    }
}


// <div class="item">
//     <div class="content">
//          <a class="header">Daniel Louise</a>
//      </div>
// </div>