console.log("inside background");

// setup extension

// var SERVER_URL = "https://recommnodeappserver.herokuapp.com";
var SERVER_URL = "http://172.16.155.158:3000";
var EXTENSION_ID;
var RECOMM_URLS = [];
var ALL_RECOMM_URLS = [];
var ALL_TRENDING = [];
var ALL_TRENDING_SIZE;

var gettingExtensionId = false;

mainBackgroundPooling();

function mainBackgroundPooling(){
    console.log("Starting background pooling");
    ensureExtensionId(onExtensionId);
    setTimeout(function () {
        mainBackgroundPooling();
    }, 1800000); // 36,00.000 == 60 mins
}

function onExtensionId(){
    getTrending(getTrendingMetaData);
    getHistory(3, true, sendHistory);
    getRecomms(updateRecomm);

}

function ensureExtensionId(callback) {
    console.log("Ensuring I have extension id set.");
    if (EXTENSION_ID) {
        console.log("Found it! Yay");
        console.log("Its " + EXTENSION_ID.toString());
        callback();
    } else {
        console.log("Shiz! its missing");
        if (!gettingExtensionId) {
            console.log("Getting Extension id");
            gettingExtensionId = true;
            getExtensionId();
        }
        if (gettingExtensionId) {
            console.log("Will check back in a sec.");
            setTimeout(function () {
                return ensureExtensionId(callback);
            }, 1000);
        }
    }
}

function getExtensionId(){
    gettingExtensionId = true;
    chrome.storage.local.get("extensionId", function (data) {
        console.log(data);
        if(Object.keys(data).length === 0){
            console.log("no data found");
            console.log(SERVER_URL + "/extensions/create");
            $.get(SERVER_URL + "/extensions/create", function (data) {
                console.log(data);
                chrome.storage.local.set({extensionId: data});
                EXTENSION_ID = data["extensionId"];
                console.log("Setting extension id: " + EXTENSION_ID.toString());
                gettingExtensionId = false;
            }).fail(function (err) {
                console.log(err);
                console.log("Error");
                gettingExtensionId = false;
            });
        } else {
            EXTENSION_ID = data["extensionId"]["extensionId"];
            console.log(EXTENSION_ID);
            gettingExtensionId = false;
        }
    });
};
// create new tab on browserAction click
chrome.browserAction.onClicked.addListener(function(activeTab) {
    chrome.tabs.create({});
});

function getHistory(timePeriod, isDays, callback){
    console.log("Getting history");
    var timeNow = Date.now();
    var timeDiff;
    if (isDays === true) // in days
        timeDiff = timePeriod * 24 * 60 * 60 * 1000;
    else  // in hours
        timeDiff = timePeriod * 60 * 60 * 1000;

    var startTime = timeNow - timeDiff;

    chrome.history.search({text: '', maxResults: 100, startTime: startTime, endTime: timeNow}, function (results) {
        console.log("History extracted, see for yourself!");
        console.log(results);
        callback(results);
    })
}

function getMostVisited(callback){
    chrome.topSites.get(function (results) {
        callback(results);
    })
}

function sendHistory(history){
    console.log("Sending history to the app server");
    var formattedHistory = [];
    history.forEach(function (t) {
        var historyItem = {};
        historyItem["url"] = t.url;
        historyItem["lastVisitTime"] = t.lastVisitTime;
        formattedHistory.push(historyItem);
    });

    console.log(formattedHistory);
    $.post(SERVER_URL + "/history", {"extensionId": EXTENSION_ID, "history" :formattedHistory}, function (res) {
        console.log("history sent and got following result back");
        console.log(res);
    })
}

function setUrlMeta(metadata){
    if (!metadata.image_url){
        metadata.image_url = metadata.icon_url;
    }
    if(!metadata.title){
        metadata.title = metadata.url;
    }
    if (metadata.title) {
        RECOMM_URLS.push(metadata);
        console.log("Storing following formatted recomms to local storage");
        console.log(RECOMM_URLS);
        chrome.storage.local.set({recommurls: RECOMM_URLS});
    }
}

var domParser = new DOMParser();

function getUrlHtml(url, callback){
    $.get(url, function (result) {
        var metadata = metadataparser.getMetadata(domParser.parseFromString(result, "text/xml"), url);
        // console.log(metadata);
        callback(metadata);
    }).fail(function (err) {
        console.log(err);
        for (var i = 0; i < ALL_RECOMM_URLS.length; i++){
            if (ALL_RECOMM_URLS[i].url = url){
                ALL_RECOMM_URLS.pop(i);
            }
        }
    });
}

function getUrlInfoAndUpdate(){
    console.log("Getting title and desc of urls");
    for(var i=0; i<ALL_RECOMM_URLS.length; i++){
        getUrlHtml(ALL_RECOMM_URLS[i].url, setUrlMeta);
    }
}

// function resolveImagesForTopRecomm(urls){
//     var resolver = new ImageResolver();
//     resolver.register(new ImageResolver.FileExtension());
//     resolver.register(new ImageResolver.MimeType());
//     resolver.register(new ImageResolver.Opengraph());
//     resolver.register(new ImageResolver.Webpage());
//
//     urls.forEach(function (url) {
//         resolver.resolve(url, function (result) {
//             if (result) {
//                 formattedUrl = {};
//                 formattedUrl.url = url;
//                 formattedUrl.imageUrl = result;
//                 RECOMM_URLS.push(formattedUrl);
//             } else {
//                 formattedUrl = {};
//                 formattedUrl.url = url;
//                 formattedUrl.imageUrl = "mathew.png";
//                 RECOMM_URLS.push(formattedUrl);
//             }
//         })
//     });
// }

function updateRecomm(urls){
    ALL_RECOMM_URLS = [];
    RECOMM_URLS = [];

    urls.forEach(function (url) {
       var formattedUrl = {};
       formattedUrl.url = url;
       ALL_RECOMM_URLS.push(formattedUrl);
    });

    getUrlInfoAndUpdate();
}

function getRecomms(callback){
    console.log("Getting recommendations for you");
    $.get(SERVER_URL + "/extensions/getrecomm", {extensionId: EXTENSION_ID }).done(function (data) {
        console.log("Got following recomms");
        console.log(data);
        if (!data.urls){
            setTimeout(function () {
                getRecomms(callback);
            }, 5000)
        }
        else{
            ALL_RECOMM_URLS = data.urls;
            callback(data.urls);
        }
    }).fail(function () {
        setTimeout(function () {
            getRecomms(callback);
        }, 5000);
    });
}

var pushed_trending_size;

function getTrending(callback){
    console.log("Getting trending");
    $.get(SERVER_URL + "/trending").done(function (data) {
        console.log("Got following trending items");
        console.log(data);
        ALL_TRENDING_SIZE = data.length;
        pushed_trending_size = 0;
        ALL_TRENDING = [];
        data.forEach(function (t) {
            callback(t.url);
        });
    })
}



function getTrendingMetaData(url) {
    pushed_trending_size++;
    $.get(url, function (result) {
        var metadata = metadataparser.getMetadata(domParser.parseFromString(result, "text/xml"), url);
        // console.log(metadata);
        if (!metadata.image_url){
            metadata.image_url = metadata.icon_url;
        }
        if(!metadata.title){
            metadata.title = metadata.url;
        }
        if (metadata.title){
            ALL_TRENDING.push(metadata);
            console.log("Set following trending items in storage");
            console.log(ALL_TRENDING);
            chrome.storage.local.set({"trending": ALL_TRENDING});
        }

    }).fail(function (err) {
        console.log(err);
        // Take a chill pill.
    });


}